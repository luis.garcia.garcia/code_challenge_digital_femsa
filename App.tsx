import React from "react";
import { Provider } from "react-redux";
import Roter from "./src/router";
import store from "./src/store";
import { useFonts } from "expo-font";

export default function () {
  const [fontLoaded] = useFonts({
    Avenir: require("./assets/fonts/AvenirLTStd-Black.otf"),
  });

  if (!fontLoaded) {
    return null;
  }

  return (
    <Provider store={store}>
      <Roter />
    </Provider>
  );
}
