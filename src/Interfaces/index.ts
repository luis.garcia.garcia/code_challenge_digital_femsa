export interface ReducerAction {
  readonly type: string;
  readonly payload: any;
}

export interface Product {
  createdAt: string;
  product: string;
  points: number;
  image: string;
  is_redemption: boolean;
  id: string;
}

export enum Filters {
  earned = "earned",
  redeemed = "redeemed",
  all = "all",
}
