import React from "react";
import { Text, View } from "react-native";
import { colors } from "../theme";

export default ({
  children,
  size = 16,
  family = "Avenir",
  weight = "400",
  color = colors.black,
  style = null,
  align = "auto",
}: any) => {
  return (
    <View>
      <Text
        style={{
          color: color,
          fontWeight: weight,
          fontSize: Number(size),
          fontFamily: family,
          textAlign: align,
          ...style,
        }}
      >
        {children}
      </Text>
    </View>
  );
};
