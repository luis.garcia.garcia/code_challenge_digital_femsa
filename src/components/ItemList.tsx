import React, { useEffect } from "react";
import { View, TouchableOpacity, StyleSheet, Image } from "react-native";
import Label from "./Label";
import { colors } from "../theme";
import moment from "moment";
import "moment/locale/es-mx";

export default ({ product, onPress }: any) => {
  const parseDate = (date: string) => {
    moment.locale("es-mx");
    return moment(date).format("DD MMMM YYYY");
  };

  return (
    <View>
      <TouchableOpacity onPress={() => onPress()} style={styles.item}>
        <View style={styles.row}>
          <View style={styles.imageColumn}>
            <Image
              source={{ uri: product.image }}
              style={{ width: 55, height: 55, borderRadius: 10 }}
            />
          </View>
          <View style={styles.productColumn}>
            <Label weight="800" size={14} style={styles.product}>
              {product.product}
            </Label>
            <Label weight="400" size={14} style={styles.product}>
              {parseDate(product.createdAt)}
            </Label>
          </View>
          <View style={styles.pointsColumn}>
            <Label
              color={product.is_redemption ? colors.red : colors.green}
              weight="800"
            >
              {product.is_redemption ? "-" : "+"}
            </Label>
            <Label
              style={{
                marginHorizontal: 3,
              }}
              weight="800"
            >
              {product.points.toLocaleString("en-US")}
            </Label>
            <Label weight="900"> {">"} </Label>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    width: "100%",
    backgroundColor: "white",
    borderRadius: 10,
    flex: 1,
    justifyContent: "center",
    marginBottom: 8,
  },
  text: {
    color: "blue",
    textAlign: "center",
    fontSize: 18,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  pointsColumn: {
    flex: 2,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  rowPoints: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  product: {
    marginLeft: 10,
  },
  productColumn: {
    flex: 4,
    paddingLeft: 10,
  },
  imageColumn: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center",
  },
});
