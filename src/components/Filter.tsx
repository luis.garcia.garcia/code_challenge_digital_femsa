import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import Button from "./Button";
import { colors } from "../theme";
import { Filters } from "../Interfaces";

export default ({ filter, setFilter }: any) => {
  if (filter != Filters.all) {
    return (
      <View style={styles.row}>
        <Button
          onPress={() => setFilter(Filters.all)}
          style={styles.buttonAll}
          text="Todos"
        ></Button>
      </View>
    );
  }
  return (
    <View style={styles.row}>
      <Button
        onPress={() => setFilter(Filters.earned)}
        style={styles.buttonAll}
        text="Ganados"
      ></Button>
      <Button
        onPress={() => setFilter(Filters.redeemed)}
        style={{ ...styles.buttonAll, marginLeft: 13 }}
        text="Canjeados"
      ></Button>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonAll: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: colors.primary,
    borderRadius: 10,
  },
  row: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});
