import React from "react";
import { TouchableOpacity } from "react-native";
import { colors } from "../theme";
import Label from "./Label";

export default ({ text, color, onPress = () => "a", style }: any) => {
  return (
    <TouchableOpacity onPress={() => onPress()} style={style}>
      <Label color={colors.white} width="800">
        {text}
      </Label>
    </TouchableOpacity>
  );
};
