import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { Products, ProductDetail } from "../screens";
import ProductDetailHeader from "../headers/ProductDetailHeader";
import { colors } from "../theme";

const AppNavigator = createStackNavigator(
  {
    Products: {
      screen: Products,
      navigationOptions: {
        headerShown: false,
      },
    },
    ProductDetail: {
      screen: ProductDetail,
      navigationOptions: {
        headerStyle: {
          backgroundColor: colors.info,
        },
        headerTitle: () => null,
        headerLeft: () => <ProductDetailHeader />,
      },
    },
  },
  {
    initialRouteName: "Products",
    defaultNavigationOptions: {
      headerBackTitleVisible: false,
    },
  }
);

export default createAppContainer(AppNavigator);
