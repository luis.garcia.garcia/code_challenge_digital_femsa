export const colors = {
  primary: "#334FFA",
  secondary: "#9B9898",
  white: "#FFFFFF",
  black: "#000000",
  green: "#00B833",
  red: "#FF0000",
  info: "#CFD6FF",
};
