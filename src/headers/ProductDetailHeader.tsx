import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { colors } from "../theme";
import { connect } from "react-redux";

const ProductDetailHeader = ({ data }: any) => {
  return <Text style={styles.headerTitle}>{data.productDetail.product}</Text>;
};

const mapStateToProps = (state: any) => {
  return { data: state.products };
};

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDetailHeader);

const styles = StyleSheet.create({
  headerTitle: {
    textAlign: "auto",
    color: colors.black,
    fontSize: 24,
    fontWeight: "800",
    marginLeft: 20,
  },
});
