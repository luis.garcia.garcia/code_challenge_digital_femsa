import axios from "axios";
import { ReducerAction, Product } from "../Interfaces";
const url = "https://6222994f666291106a29f999.mockapi.io/api/v1/products";

const types = {
  SET_PRODUCTS: "SET_PRODUCTS",
  SET_PRODUCTS_START: "SET_PRODUCTS_START",
  SET_PRODUCTS_ERROR: "SET_PRODUCTS_ERROR",
  SET_PRODUCT_DETAIL: "SET_PRODUCT_DETAIL",
  SET_TOTAL_POINTS: "SET_TOTAL_POINTS",
};

const initialState = {
  loading: false,
  products: [],
  error: null,
  productDetail: {
    product: "",
  },
  totalPoints: 0,
};

const setProducts = (products: any[]) => ({
  type: types.SET_PRODUCTS,
  payload: products,
});

const setProductsStart = () => ({
  type: types.SET_PRODUCTS_START,
});

const setProductserror = (error: any) => ({
  type: types.SET_PRODUCTS_ERROR,
  payload: error,
});

export const setProductDetail = (product: Product) => ({
  type: types.SET_PRODUCT_DETAIL,
  payload: product,
});

export const setTotalPoints = (points: number) => ({
  type: types.SET_TOTAL_POINTS,
  payload: points,
});

export default (state = initialState, action: ReducerAction) => {
  switch (action.type) {
    case types.SET_PRODUCTS:
      return {
        ...state,
        loading: false,
        error: null,
        products: action.payload,
      };
    case types.SET_PRODUCTS_START:
      return {
        ...state,
        loading: true,
      };
    case types.SET_PRODUCTS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.SET_PRODUCT_DETAIL:
      return {
        ...state,
        productDetail: action.payload,
      };
    case types.SET_TOTAL_POINTS:
      return {
        ...state,
        totalPoints: action.payload,
      };
    default:
      return state;
  }
};

export const getProducts = () => async (dispatch: any, getState) => {
  dispatch(setProductsStart());
  axios
    .get(url)
    .then((response: any) => {
      let total = 0;
      response.data.map((item: Product) => {
        if (item.is_redemption == false) total += item.points;
      });
      dispatch(setTotalPoints(total));
      dispatch(setProducts(response.data));
    })
    .catch((error) => {
      dispatch(setProductserror(error.message));
    });
};
