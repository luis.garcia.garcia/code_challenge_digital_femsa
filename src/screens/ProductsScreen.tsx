import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  ActivityIndicator,
  SafeAreaView,
  Dimensions,
  FlatList,
} from "react-native";
import { getProducts, setProductDetail } from "../reducers/products";
import { connect } from "react-redux";
import { Product } from "../Interfaces";
import { Label, ItemList, Filter } from "../components";
import { colors } from "../theme";
import { Filters } from "../Interfaces";

const ProductsScreen = ({
  navigation,
  data,
  getProducts,
  setProductDetail,
}: any) => {
  const [filter, setFilter] = useState(Filters.all);

  const setDetail = (product: Product) => {
    setProductDetail(product);
    navigation.navigate("ProductDetail");
  };

  useEffect(() => {
    getProducts();
  }, []);

  function filterIsRedemption(product: Product) {
    if (filter == Filters.earned) {
      return !product.is_redemption;
    }

    return product.is_redemption;
  }

  const filteredProducts = () => {
    if (filter != Filters.all) {
      return data.products.filter(filterIsRedemption);
    }

    return data.products;
  };

  if (data.loading) {
    return (
      <View style={styles.containerCentered}>
        <ActivityIndicator size="large" color="#000" />
        <Label>Cargando productos....</Label>
      </View>
    );
  }

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Label size={20} weight="800">
          Bienvenido de vuelta!
        </Label>
        <Label>Ruben Rodriguez</Label>
        <Label
          style={styles.subtitle}
          size={14}
          color={colors.secondary}
          weight="800"
        >
          TUS PUNTOS
        </Label>
        <View style={styles.pointer}>
          <Label
            style={styles.pointerTitle}
            size={16}
            weight="800"
            color={colors.white}
          >
            Diciembre
          </Label>
          <Label align="center" size={32} weight="800" color={colors.white}>
            {data.totalPoints.toLocaleString("en-US")} pts
          </Label>
        </View>
        <Label
          style={styles.subtitle}
          size={14}
          color={colors.secondary}
          weight="800"
        >
          TUS MOVIMIENTOS
        </Label>
        <View style={styles.listContainer}>
          <FlatList
            data={filteredProducts()}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => (
              <ItemList onPress={() => setDetail(item)} product={item} />
            )}
            style={styles.list}
          />
        </View>
        <Filter filter={filter} setFilter={setFilter}></Filter>
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = (state: any) => {
  return { data: state.products };
};

const mapDispatchToProps = (dispatch: any) => ({
  getProducts: () => dispatch(getProducts()),
  setProductDetail: (product: Product) => dispatch(setProductDetail(product)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductsScreen);

const styles = StyleSheet.create({
  containerCentered: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  container: {
    padding: 20,
    paddingTop: 40,
  },
  subtitle: {
    marginVertical: 20,
  },
  pointer: {
    backgroundColor: colors.primary,
    shadowColor: "rgba(0,0,0,0.5)",
    shadowOffset: {
      width: 4,
      height: 4,
    },
    width: Dimensions.get("window").width / 1.3,
    borderRadius: 20,
    alignSelf: "center",
    height: 143,
  },
  pointerTitle: {
    marginLeft: 20,
    marginTop: 20,
    marginBottom: 7,
  },
  list: {
    backgroundColor: "white",
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 23,
  },
  listContainer: {
    height: Dimensions.get("window").height / 2.3,
    marginBottom: 30,
  },
});
