import { StatusBar } from "expo-status-bar";
import { StyleSheet, View, Image, Dimensions } from "react-native";
import { connect } from "react-redux";
import { Product } from "../Interfaces";
import { setProductDetail } from "../reducers/products";
import { Label, Button } from "../components";
import { colors } from "../theme";
import moment from "moment";
import "moment/locale/es-mx";

const ProductDetailScreen = ({ navigation, data }: any) => {
  const parseDate = (date: string) => {
    moment.locale("es-mx");
    return moment(date).format("DD MMMM YYYY");
  };

  const navigateToProducts = () => {
    navigation.navigate("Products");
  };

  return (
    <View style={styles.container}>
      <Image
        source={{ uri: data.productDetail.image }}
        style={{
          borderRadius: 10,
          width: Dimensions.get("window").width - 40,
          height: 350,
        }}
      />
      <Label
        style={{ marginTop: 32 }}
        color={colors.secondary}
        weight="800"
        size={14}
      >
        Detalles de producto
      </Label>
      <Label style={{ marginTop: 19 }} weight="800" size={16}>
        Comprado el {parseDate(data.productDetail.createdAt)}
      </Label>
      <Label
        style={{ marginTop: 20 }}
        color={colors.secondary}
        weight="800"
        size={14}
      >
        Con esta compra acumulaste
      </Label>
      <Label style={{ marginTop: 32 }} weight="800" size={24}>
        {data.productDetail.points.toLocaleString("en-US")} puntos
      </Label>
      <View style={styles.row}>
        <Button
          onPress={() => navigateToProducts()}
          style={styles.button}
          text="Aceptar"
        ></Button>
      </View>
    </View>
  );
};

const mapStateToProps = (state: any) => {
  return { data: state.products };
};

const mapDispatchToProps = (dispatch: any) => ({
  setProductDetail: (product: Product) =>
    dispatch(setProductDetail(data.productDetail)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDetailScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20,
  },
  button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: colors.primary,
    borderRadius: 10,
  },
  row: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 40,
    left: 20,
    right: 20,
  },
});
